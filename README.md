# ERT-CLOCK for Angular 2

## Overview
Angular 2 component that shows a clock set on the timezone provided as component input.  
This component time is Daylight Saving Time aware.
AoT ready.


## How to use it

1. Install with <code>npm install ng2-ert-clock</code>
2. Integrate <code>node_modules/ng2-ert-clock/ert-clock.css</code> or <code>node_modules/ng2-ert-clock/src/ert-clock.scss</code> in your CSS/SCSS.
3. Optional - Import <code>node_modules/ng2-ert-clock/quadrant1.png</code> in your asset folder
4. If needed, overwrite CSS rule on <code>.ert-clock</code> in order to use fix the desired quadrant as background-image
5. Import the Angular 2 module into your module using <code>import { ErtClockModule } from 'ng2-ert-clock';</code> and put <code>ErtClockModule</code> in your module <code>imports</code>

    Example:

    <code>
    import { ErtClockModule } from 'ng2-ert-clock';

    @NgModule({
        imports: [ErtClockModule],
        ...
    </code>
6. Use the clock adding a <code>ert-clock</code> element in your template with a [valid momentJsPlace input](http://momentjs.com/timezone/ "Moment Timezone")
    
    Example:

    <ert-clock [momentJsPlace]="'America/Los_Angeles'"></ert-clock>  
    <ert-clock [momentJsPlace]="'Europe/Rome'"></ert-clock>


## Demo
Demo available: [https://bitbucket.org/eurisredteam/ng2-ert-clock-demo](https://bitbucket.org/eurisredteam/ng2-ert-clock-demo "demo repository")


## Questions?
Feel free to contact the author!