import { Component, OnInit, Input, ElementRef } from '@angular/core';

import * as moment from 'moment';
import 'moment-timezone'

@Component({
    selector: 'ert-clock',
    template: `<div class="ert-clock">
                    <div class="ert-clock-center"></div>
                    <div class="ert-clock-hours-container">
                        <div class="ert-clock-hours"></div>
                    </div>
                    <div class="ert-clock-minutes-container">
                        <div class="ert-clock-minutes"></div>
                    </div>
                    <div class="ert-clock-seconds-container">
                        <div class="ert-clock-seconds"></div>
                    </div>
                </div>`
})
export class ErtClockComponent implements OnInit {
    @Input() momentJsPlace: string = '';

    constructor(private el: ElementRef) { }

    ngOnInit(): void {
        //Set initial time with timezone and DST aware
        var now = (moment as any).tz(this.momentJsPlace);
        var hours = now.hours();
        var minutes = now.minutes();
        var seconds = now.seconds();

        // Create an object with each hand and it's angle in degrees
        var hands = [
            {
                handClass: 'ert-clock-hours',
                angle: (hours * 30) + (minutes / 2) - 90
            },
            {
                handClass: 'ert-clock-minutes',
                angle: (minutes * 6) - 90
            },
            {
                handClass: 'ert-clock-seconds',
                angle: (seconds * 6) - 90
            }
        ];

        // Loop through each of these hands to set their angle
        for (var j = 0; j < hands.length; j++) {
            let element = this.el.nativeElement.getElementsByClassName(hands[j].handClass)[0];
            let angle = hands[j].angle;
            element.style.webkitTransform = 'rotateZ(' + angle + 'deg)';
            element.style.transform = 'rotateZ(' + angle + 'deg)';
        }
    }
}