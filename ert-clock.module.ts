import {NgModule, ModuleWithProviders} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ErtClockComponent} from "./src/ert-clock.component";

export * from './src/ert-clock.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ErtClockComponent
  ],
  exports: [
    ErtClockComponent
  ]
})
export class ErtClockModule {}